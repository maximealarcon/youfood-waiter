package com.rman.youfoodwaiter.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodwaiter.entity.Instruction;

public class JsonInstructionConverter {

	public static final String ID_FIELD = "id";
	public static final String DATE_FIELD = "creationDate";
	public static final String MENUS_FIELD = "menus";
	public static final String TABLE_FIELD = "table";
	public static final String SERVED_FIELD = "served";
	
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	public static List<Instruction> JsonToInstructionList(JSONObject json) throws JSONException {
		ArrayList<Instruction> instructionList = new ArrayList<Instruction>();
		if(json.has("instruction")){
			JSONArray instructionElements = json.optJSONArray("instruction");
			if(instructionElements == null){
				//Un résultat
				instructionList.add(JsonInstructionConverter.JsonToInstruction(json.optJSONObject("instruction")));
			} else {
				//Plusieurs résultats
				for (int i = 0; i < instructionElements.length(); i++){
					instructionList.add(JsonInstructionConverter.JsonToInstruction(instructionElements.getJSONObject(i)));
				}
			}
		}
		return instructionList;
	}
	
	public static Instruction JsonToInstruction(JSONObject json) throws JSONException {
		Instruction instruction = new Instruction();
		instruction.setId(json.getLong(ID_FIELD));
		try {
			instruction.setCreationDate(dateFormat.parse(json.getString(DATE_FIELD)));
		} catch (ParseException e){}
		instruction.setMenus(JsonInstructionMenuConverter.JsonToInstructionMenuList(json));
		instruction.setTable(JsonTabulaConverter.JsonToTabula(json.getJSONObject(TABLE_FIELD)));
		instruction.setServed(json.getBoolean(SERVED_FIELD));
		
		return instruction;
	}
}
