package com.rman.youfoodwaiter.converter;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodwaiter.entity.InstructionMenu;

public class JsonInstructionMenuConverter {

	public static final String ID_FIELD = "id";
	public static final String STARTER_FIELD = "starter";
	public static final String PRINCIPAL_FIELD = "principal";
	public static final String DESERT_FIELD = "desert";
	
	
	public static List<InstructionMenu> JsonToInstructionMenuList(JSONObject json) throws JSONException {
		ArrayList<InstructionMenu> menusList = new ArrayList<InstructionMenu>();
		if(json.has("menus")){
			JSONArray menusElements = json.optJSONArray("menus");
			if(menusElements == null){
				//Un résultat
				menusList.add(JsonInstructionMenuConverter.JsonToInstructionMenu(json.optJSONObject("menus")));
			} else {
				//Plusieurs résultats
				for (int i = 0; i < menusElements.length(); i++){
					menusList.add(JsonInstructionMenuConverter.JsonToInstructionMenu(menusElements.getJSONObject(i)));
				}
			}
		}
		return menusList;
	}
	
	public static InstructionMenu JsonToInstructionMenu(JSONObject json) throws JSONException {
		InstructionMenu menu = new InstructionMenu();
		menu.setStarter(json.optJSONObject(STARTER_FIELD) != null ? JsonProductConverter.JsonToProduct(json.getJSONObject(STARTER_FIELD)) : null);
		menu.setPrincipal(json.optJSONObject(PRINCIPAL_FIELD) != null ? JsonProductConverter.JsonToProduct(json.getJSONObject(PRINCIPAL_FIELD)) : null);
		menu.setDesert(json.optJSONObject(DESERT_FIELD) != null ? JsonProductConverter.JsonToProduct(json.getJSONObject(DESERT_FIELD)) : null);
		return menu;
	}
}
