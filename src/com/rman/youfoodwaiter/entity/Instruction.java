package com.rman.youfoodwaiter.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class Instruction {
	private Long id;
	private Date creationDate;
	private List<InstructionMenu> menus;
	private Tabula table;
	private boolean served;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public List<InstructionMenu> getMenus() {
		return menus;
	}
	public void setMenus(List<InstructionMenu> menus) {
		this.menus = menus;
	}
	public Tabula getTable() {
		return table;
	}
	public void setTable(Tabula table) {
		this.table = table;
	}
	public boolean getServed() {
		return served;
	}
	public void setServed(boolean served) {
		this.served = served;
	}
	
	public Float getPrice(){
		Float price = new Float(0);
		if(this.menus != null && this.menus.size() > 0){
			for(int i = 0; i < this.menus.size(); i++){
				price += this.menus.get(i).getPrice();
			}
		}
		return price;
	}
	
	public String getStringDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy' � 'HH:mm");
		return sdf.format(this.creationDate);
	}
	
	public String getElapsedTime(){
		String result = "";
		Date elapsedTime = new Date(new Date().getTime() - this.creationDate.getTime());
		elapsedTime.setHours(elapsedTime.getHours() - 1);
		if(elapsedTime.getDate() == 1){
			if(elapsedTime.getHours() == 0){
				if(elapsedTime.getMinutes() == 0){
					result = "Moins d'une minute";
				} else {
					result = "Depuis " + elapsedTime.getMinutes() + " minute(s)";
				}
			} else {
				result = "Depuis " + (elapsedTime.getHours()) + " heure(s)";
			}
		} else {
			result = "Plus de 24 heures";
		}
		return result;
	}
}
