package com.rman.youfoodwaiter.entity;

import java.util.List;


public class Restaurant {
	private Long id;
	private String name;
	private List<Zone> zones;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Zone> getZones() {
		return zones;
	}
	public void setZones(List<Zone> zones) {
		this.zones = zones;
	}
}
