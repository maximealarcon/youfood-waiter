package com.rman.youfoodwaiter.entity;

public class InstructionMenu {
	
	private Long id;
	private Product starter;
	private Product principal;
	private Product desert;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Product getStarter() {
		return starter;
	}
	public void setStarter(Product starter) {
		this.starter = starter;
	}
	public Product getPrincipal() {
		return principal;
	}
	public void setPrincipal(Product principal) {
		this.principal = principal;
	}
	public Product getDesert() {
		return desert;
	}
	public void setDesert(Product desert) {
		this.desert = desert;
	}
	
	public Float getPrice(){
		Float price = new Float(0);
		if(this.starter != null) price += starter.getPrice();
		if(this.principal != null) price += principal.getPrice();
		if(this.desert != null) price += desert.getPrice();
		return price;
	}
}
