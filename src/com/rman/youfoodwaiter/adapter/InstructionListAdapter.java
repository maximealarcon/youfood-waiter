package com.rman.youfoodwaiter.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rman.youfoodwaiter.activity.R;
import com.rman.youfoodwaiter.entity.Instruction;

public class InstructionListAdapter extends BaseAdapter {

	private List<Instruction> objects;
	private LayoutInflater inflater;
	
	public InstructionListAdapter(Context context, List<Instruction> objects) {
		this.objects = objects;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public Instruction getItem(int position) {
		return objects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return this.createViewFromResource(position, convertView, parent);
	}
	
	private View createViewFromResource(int position, View convertView, ViewGroup parent){
		View view;
        
        if (convertView == null) {
            view = inflater.inflate(R.layout.instructionlistrow, parent, false);
        } else {
            view = convertView;
        }
        
        TextView title = (TextView) view.findViewById(R.id.instructionlistrowTitle);
        TextView subtitle = (TextView) view.findViewById(R.id.instructionlistrowSubtitle);
        
        Instruction i = getItem(position);
        title.setText("#" + i.getId() + " - Table " + i.getTable().getName() + " - " + i.getMenus().size() + " menu(s)");
        subtitle.setText(i.getElapsedTime());
        
        
        return view;
	}
}
