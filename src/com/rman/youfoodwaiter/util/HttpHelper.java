package com.rman.youfoodwaiter.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class HttpHelper {

	public static String API_URI = "http://localhost:8080/YouFood/resources";
	public static int timeout = 10000;
	
	public static void setURI(String address){
		HttpHelper.API_URI = "http://"+address+":8080/YouFood/resources";
	}
	
	public static String get(String address) throws ResourceNotFoundException {
		String result = null;
    	try {
    		//Timeout
    		HttpParams httpParams = new BasicHttpParams();
    		HttpConnectionParams.setConnectionTimeout(httpParams, HttpHelper.timeout);
    		HttpConnectionParams.setSoTimeout(httpParams, HttpHelper.timeout);
    		
    		//Cr�ation du client de mise en forme de la requete (JSON)
    		HttpClient httpClient = new DefaultHttpClient(httpParams);
    		HttpGet httpGet = new HttpGet();
    		URI uri = new URI(address);
    		if(uri.getHost() == null){
    			throw new ResourceNotFoundException("Invalid host", address);
    		}
    		httpGet.setURI(uri);
    		httpGet.setHeader("Accept", "application/json");
    		
    		//Ex�cution
    		HttpResponse response = httpClient.execute(httpGet);
    		int statusCode = response.getStatusLine().getStatusCode();
    		
    		if(statusCode != 200){
    			throw new ResourceNotFoundException(statusCode, address);
    		} else {
    			result = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
    		}
    	} catch (IOException e){
    		throw new ResourceNotFoundException("Impossible de se connecter au service", address);
    	} catch (URISyntaxException u){
    		throw new ResourceNotFoundException("Impossible de se connecter au service", address);
    	}
    	return result;
	}
	
	
	public static String post(String address, JSONObject json) throws ResourceNotFoundException {
		String result = null;
    	try {
    		//Timeout
    		HttpParams httpParams = new BasicHttpParams();
    		HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
    		HttpConnectionParams.setSoTimeout(httpParams, 5000);
    		
    		//Cr�ation du client de mise en forme de la requete (JSON)
    		HttpClient httpClient = new DefaultHttpClient(httpParams);
    		HttpPost httpPost = new HttpPost();
    		URI uri = new URI(address);
    		if(uri.getHost() == null){
    			throw new ResourceNotFoundException("Invalid host", address);
    		}
    		httpPost.setURI(uri);
    		httpPost.setHeader("Accept", "application/json");
    		httpPost.setEntity(new StringEntity(json.toString()));
    		
    		//Ex�cution
    		HttpResponse response = httpClient.execute(httpPost);
    		int statusCode = response.getStatusLine().getStatusCode();

    		if(statusCode != 200){
    			throw new ResourceNotFoundException(statusCode, address);
    		} else {
    			result = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
    		}
    	} catch (IOException e){
    		throw new ResourceNotFoundException("Impossible de se connecter au service", address);
    	} catch (URISyntaxException u){
    		throw new ResourceNotFoundException("Impossible de se connecter au service", address);
    	}
    	return result;
	}
	
	public static String put(String address, JSONObject json) throws ResourceNotFoundException {
		String result = null;
    	try {
    		//Timeout
    		HttpParams httpParams = new BasicHttpParams();
    		HttpConnectionParams.setConnectionTimeout(httpParams, 5000);
    		HttpConnectionParams.setSoTimeout(httpParams, 5000);
    		
    		//Cr�ation du client de mise en forme de la requete (JSON)
    		HttpClient httpClient = new DefaultHttpClient(httpParams);
    		HttpPut httpPut = new HttpPut();
    		URI uri = new URI(address);
    		if(uri.getHost() == null){
    			throw new ResourceNotFoundException("Invalid host", address);
    		}
    		httpPut.setURI(uri);
    		httpPut.setHeader("Accept", "application/json");
    		httpPut.setEntity(new StringEntity(json.toString()));
    		
    		//Ex�cution
    		HttpResponse response = httpClient.execute(httpPut);
    		int statusCode = response.getStatusLine().getStatusCode();

    		if(statusCode != 200){
    			throw new ResourceNotFoundException(statusCode, address);
    		} else {
    			result = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
    		}
    	} catch (IOException e){
    		throw new ResourceNotFoundException("Impossible de se connecter au service", address);
    	} catch (URISyntaxException u){
    		throw new ResourceNotFoundException("Impossible de se connecter au service", address);
    	}
    	return result;
	}
}
