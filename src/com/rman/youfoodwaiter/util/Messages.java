package com.rman.youfoodwaiter.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Messages {

	public static int ERROR = android.R.drawable.ic_dialog_alert;
	public static int INFO = android.R.drawable.ic_dialog_info;
	
	public static void createAlert(Context context, String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
               .setCancelable(false)
               .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                   }
               });
        AlertDialog alert = builder.create();
        alert.show();
	}
	
	public static void createAlert(Context context, String title, int alertType, String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
        	   .setIcon(alertType)
        	   .setMessage(message)
               .setCancelable(false)
               .setNegativeButton("Fermer", new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                   }
               });
        AlertDialog alert = builder.create();
        alert.show();
	}
}
