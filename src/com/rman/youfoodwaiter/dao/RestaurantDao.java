package com.rman.youfoodwaiter.dao;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodwaiter.converter.JsonRestaurantConverter;
import com.rman.youfoodwaiter.entity.Restaurant;
import com.rman.youfoodwaiter.util.HttpHelper;
import com.rman.youfoodwaiter.util.ResourceNotFoundException;

public class RestaurantDao {

private static String API_RESTAURANT_RELATIVE_URI = "/restaurant";
	
	public static List<Restaurant> getAllRestaurants() throws ResourceNotFoundException, JSONException {
		String response = HttpHelper.get(HttpHelper.API_URI + API_RESTAURANT_RELATIVE_URI + "/all");
		return response.equals("null") ? null : JsonRestaurantConverter.JsonToRestaurantList(new JSONObject(response));
	}
	
	public static Restaurant getRestaurantById(List<Restaurant> restaurants, Long id){
		for(int i = 0; i < restaurants.size(); i++){
			if(restaurants.get(i).getId() == id) return restaurants.get(i);
		}
		return null;
	}
}
