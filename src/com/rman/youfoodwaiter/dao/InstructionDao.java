package com.rman.youfoodwaiter.dao;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.rman.youfoodwaiter.converter.JsonInstructionConverter;
import com.rman.youfoodwaiter.entity.Instruction;
import com.rman.youfoodwaiter.util.HttpHelper;
import com.rman.youfoodwaiter.util.ResourceNotFoundException;

public class InstructionDao {

	private static String API_INSTRUCTION_RELATIVE_URI = "/instruction";
	
	public static List<Instruction> getInstructionsForZone(Long idZone) throws ResourceNotFoundException, JSONException {
		String response = HttpHelper.get(HttpHelper.API_URI + API_INSTRUCTION_RELATIVE_URI + "/"+idZone);
		return response.equals("null") ? null : JsonInstructionConverter.JsonToInstructionList(new JSONObject(response));
	}
	
	public static Instruction getInstructionById(Long idInstruction) throws ResourceNotFoundException, JSONException {
		String response = HttpHelper.get(HttpHelper.API_URI + API_INSTRUCTION_RELATIVE_URI + "/one/"+idInstruction);
		return response.equals("null") ? null : JsonInstructionConverter.JsonToInstruction(new JSONObject(response));
	}
	
	public static void finishInstruction(Long instructionId) throws ResourceNotFoundException, JSONException {
		HttpHelper.put(HttpHelper.API_URI + API_INSTRUCTION_RELATIVE_URI + "/finish/"+instructionId, new JSONObject());
	}
}
