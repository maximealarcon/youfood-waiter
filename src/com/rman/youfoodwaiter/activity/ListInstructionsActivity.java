package com.rman.youfoodwaiter.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.rman.youfoodwaiter.adapter.InstructionListAdapter;
import com.rman.youfoodwaiter.dao.InstructionDao;
import com.rman.youfoodwaiter.entity.Instruction;
import com.rman.youfoodwaiter.util.HttpHelper;
import com.rman.youfoodwaiter.util.Messages;
import com.rman.youfoodwaiter.util.ResourceNotFoundException;

public class ListInstructionsActivity extends Activity {
    
	//List
	private List<Instruction> instructions;
	private ListView instructionsListView;
	
	//Timer
	Timer timer;
	final Handler handler = new Handler();
	TimerTask repeatTask;
	
	//Infos
	private Long zone;
	private final int refreshDelay = 15000;
	
	//Dialog
	private ProgressDialog pd;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listinstructions);
        
      	//Creation du ProgressDialog
  		this.pd = new ProgressDialog(this);
  		this.pd.setCancelable(false);
  		
  		//Récupération des infos
  		this.instructionsListView = (ListView) findViewById(R.id.instructionsListView);
  		this.instructions = new ArrayList<Instruction>();
  		
  		//Clique sur une commande
  		this.instructionsListView.setOnItemClickListener(new OnItemClickListener() {
  			@SuppressWarnings("rawtypes")
			@Override
			public void onItemClick(AdapterView a, View v, int position, long id) {
				Intent intent = new Intent(ListInstructionsActivity.this, InstructionDetailsActivity.class);
				intent.putExtra("instructionId", instructions.get(position).getId());
				startActivityForResult(intent, 1);
			}
		});
  		
  		//Chargement des préférences
  		this.loadPreferences();
  		if(this.zone != 0){
  			this.getInstructions(this.zone, true);
  		}
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	
    	cancelSchedule();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	scheduleGetInstructions();
    }
    
    private void loadPreferences(){
    	SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
  		HttpHelper.setURI(sp.getString("address", "localhost"));
        this.zone = Long.valueOf(sp.getString("zone", "0"));
        if(this.zone <= 0){
        	Toast.makeText(this, "Aucune zone n'est configurée.\nVeuillez configurer votre application.", Toast.LENGTH_LONG).show();
        	Intent intent = new Intent(ListInstructionsActivity.this, ConfigActivity.class);
        	startActivityForResult(intent, 2);
        }
    }
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.layout.listinstructions_menu, menu);
    	
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case R.id.refreshoption:
			this.timer.cancel();
			this.getInstructions(this.zone, true);
			this.scheduleGetInstructions();
			return true;
		case R.id.configoption:
			Intent intent = new Intent(ListInstructionsActivity.this, ConfigActivity.class);
        	startActivityForResult(intent, 2);
        	return true;
		}
    	return false;
    }
    
    
    
    private void scheduleGetInstructions(){
    	this.timer = new Timer();
    	this.repeatTask = new TimerTask() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	            handler.post(new Runnable() {
	                public void run() {
	                    try {
	                    	getInstructions(zone, false);
	                    } catch (Exception e) {
	                        
	                    }
	                }
	            });
	        }
	    };
	    this.timer.schedule(this.repeatTask, this.refreshDelay,this.refreshDelay);
    }
    
    
    private void cancelSchedule(){
    	this.timer.cancel();
    }
    
    
    private void notifyAdapter(){
    	if(this.instructions.size() > 0){
			if(this.instructionsListView.getAdapter() != null){
				((InstructionListAdapter) this.instructionsListView.getAdapter()).notifyDataSetChanged();
			} else {
				this.instructionsListView.setAdapter(new InstructionListAdapter(this ,this.instructions));
			}
		} else {
			this.instructionsListView.setAdapter(null);
		}
    }
    
    //Tasks
  	//Get instructions
  	private void getInstructions(final Long idZone, final boolean showPd){
  		new AsyncTask<Void, Void, List<Instruction>>(){

  			private String errorMessage;
  			
  			@Override
  			protected List<Instruction> doInBackground(Void... params) {
  				List<Instruction> instructionList = null;
  	    		try {
  	    			instructionList = InstructionDao.getInstructionsForZone(idZone);
  	    		} catch (ResourceNotFoundException e) {
  	    			this.errorMessage = e.getMessage();
  	    			this.cancel(true);
  	    		} catch (JSONException j) {
  	    			this.errorMessage = "Erreur pendant la récupération des données";
  	    			this.cancel(true);
  	    		}
  	    		return instructionList;
  			}
  			
  			protected void onPreExecute() {
  				if(showPd){
	  				pd.setMessage("Chargement des commandes...\nVeuillez patienter...");
	  				pd.show();
  				}
  			}
  			
  			protected void onPostExecute(List<Instruction> result){
  				if(isCancelled()){
  					if(showPd) pd.hide();
  	  				Messages.createAlert(ListInstructionsActivity.this, "Erreur", Messages.ERROR, errorMessage);
  	  				timer.cancel();
  				} else {
  					if(showPd) pd.hide();
  	  				if(result != null) {
  	  					instructions.clear();
  	  					instructions.addAll(result);
  	  				}
  	  				notifyAdapter();
  				}
  			}
  			
  		}.execute();
  	}
  	
  	
  	@Override
  	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  		super.onActivityResult(requestCode, resultCode, data);
  		this.loadPreferences();
  		if(this.zone != 0){
  			this.getInstructions(zone, true);
  		}
  	}
}