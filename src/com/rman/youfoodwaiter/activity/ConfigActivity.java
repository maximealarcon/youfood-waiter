package com.rman.youfoodwaiter.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;

import com.rman.youfoodwaiter.dao.RestaurantDao;
import com.rman.youfoodwaiter.entity.Restaurant;
import com.rman.youfoodwaiter.util.HttpHelper;
import com.rman.youfoodwaiter.util.Messages;
import com.rman.youfoodwaiter.util.ResourceNotFoundException;

public class ConfigActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {

	//Interface
	EditTextPreference address;
	ListPreference restaurant;
	ListPreference zone;
	
	//Lists
	List<Restaurant> restaurants;
	
	//Dialog
	ProgressDialog pd;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.config);
		
		this.pd = new ProgressDialog(this);
		this.pd.setCancelable(false);
		SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
		
		//R�cup�ration des �lements
		this.address = (EditTextPreference) findPreference("address");
		this.restaurant = (ListPreference) findPreference("restaurant");
		this.zone = (ListPreference) findPreference("zone");
		
		//Traitement au lancement de l'activit�
		this.address.setSummary(sp.getString("address", "Non renseign�e"));
		this.restaurant.setSummary("Aucun");
		this.zone.setSummary("Aucune");
		this.restaurant.setEnabled(false);
		this.zone.setEnabled(false);
		
		this.restaurants = new ArrayList<Restaurant>();
		
		if(!this.address.getSummary().equals("Non renseign�e")){
			HttpHelper.setURI(this.address.getSummary().toString());
			this.getRestaurants();
		}
	}
	
	protected void onResume() {
	    super.onResume();
	    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener( this );
	}

	protected void onPause() {
	    super.onPause();
	    getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener( this );
	}

	
	
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	    //Preference pref = findPreference(key);
	    if(key.equals("address")){
	    	this.address.setSummary(this.address.getText());
	    	HttpHelper.setURI(this.address.getSummary().toString());
			this.getRestaurants();
	    } else if(key.equals("restaurant")){
	    	this.restaurant.setSummary(this.restaurant.getEntry());
	    	this.setZones(Long.valueOf(sharedPreferences.getString("restaurant", "0")));
	    } else if(key.equals("zone")){
	    	this.zone.setSummary(this.zone.getEntry());
	    }
	}
	
	
	//Task
	//Get all restaurants
	private void getRestaurants(){
  		new AsyncTask<Void, Void, List<Restaurant>>(){

  			private String errorMessage;
  			
  			@Override
  			protected List<Restaurant> doInBackground(Void... params) {
  				List<Restaurant> restaurantList = null;
  	    		try {
  	    			restaurantList = RestaurantDao.getAllRestaurants();
  	    		} catch (ResourceNotFoundException e) {
  	    			this.errorMessage = e.getMessage();
  	    			this.cancel(true);
  	    		} catch (JSONException j) {
  	    			this.errorMessage = "Erreur pendant la r�cup�ration des donn�es";
  	    			this.cancel(true);
  	    		}
  	    		return restaurantList;
  			}
  			
  			protected void onPreExecute() {
  				pd.setMessage("Chargement de la liste des restaurants...\nVeuillez patienter...");
  				pd.show();
  			}
  			
  			protected void onPostExecute(List<Restaurant> result){
  				pd.hide();
  				restaurants.clear();
  				if(isCancelled()){
  					disableRestaurants();
  					Messages.createAlert(ConfigActivity.this, "Erreur", Messages.ERROR, errorMessage);
  				} else {
  	  				if(result != null && result.size() > 0) {
  	  					restaurants.addAll(result);
  	  				}
  	  				restaurant.setEnabled(true);
  				}
  				setRestaurants();
  			}
  			
  		}.execute();
  	}
	
	private void setRestaurants(){
		//Prepare entries for selection
		CharSequence[] entries = new CharSequence[this.restaurants.size()+1];
		CharSequence[] values = new CharSequence[this.restaurants.size()+1];
		entries[0] = "Aucun";
		values[0] = "0";
		for(int i = 0; i < this.restaurants.size(); i++){
			entries[i+1] = this.restaurants.get(i).getName();
			values[i+1] = this.restaurants.get(i).getId().toString();
		}
		this.restaurant.setEntries(entries);
		this.restaurant.setEntryValues(values);
		
		//Set current value if possible
		String value = getPreferenceScreen().getSharedPreferences().getString("restaurant", "0");
		if(Arrays.binarySearch(this.restaurant.getEntryValues(), value) >= 0){
			this.restaurant.setValue(value);
		} else {
			this.restaurant.setValue("0");
		}
		this.restaurant.setSummary(this.restaurant.getEntry());
		
		this.setZones(Long.valueOf(this.restaurant.getValue()));
	}
	
	private void setZones(Long idRestaurant){
		//Try to fetch restaurant
		Restaurant r = RestaurantDao.getRestaurantById(this.restaurants, idRestaurant);
		
		//Prepare entries for selection
		CharSequence[] entries;
		CharSequence[] values;
		if(r != null){
			entries = new CharSequence[r.getZones().size()+1];
			values = new CharSequence[r.getZones().size()+1];
			for(int i = 0; i < r.getZones().size(); i++){
				entries[i+1] = r.getZones().get(i).getName();
				values[i+1] = r.getZones().get(i).getId().toString();
			}
		} else {
			entries = new CharSequence[1];
			values = new CharSequence[1];
		}
		entries[0] = "Aucune";
		values[0] = "0";
		this.zone.setEntries(entries);
		this.zone.setEntryValues(values);
		
		//Set current value if possible
		String value = getPreferenceScreen().getSharedPreferences().getString("zone", "0");
		if(Arrays.binarySearch(this.zone.getEntryValues(), value) >= 0){
			this.zone.setValue(value);
		} else {
			this.zone.setValue("0");
		}
		
		this.zone.setSummary(this.zone.getEntry());
		this.zone.setEnabled(r == null ? false : true);
	}
	
	private void disableRestaurants(){
		this.restaurant.setEnabled(false);
		this.zone.setEnabled(false);
	}
}
