package com.rman.youfoodwaiter.activity;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.rman.youfoodwaiter.adapter.InstructionMenuListAdapter;
import com.rman.youfoodwaiter.dao.InstructionDao;
import com.rman.youfoodwaiter.entity.Instruction;
import com.rman.youfoodwaiter.util.Messages;
import com.rman.youfoodwaiter.util.ResourceNotFoundException;

public class InstructionDetailsActivity extends Activity {
	
	//Instruction
	Instruction instruction;
	ListView instructiondetailsMenuListView;
	
	//Dialog
	private ProgressDialog pd;
	
	//Text
	private TextView title;
	private TextView table;
	private TextView date;
	
	//Button
	private Button finishInstructionButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.instructiondetails);
		
		//Creation du ProgressDialog
  		this.pd = new ProgressDialog(this);
  		this.pd.setCancelable(false);
  		
  		//R�cup�ration des �l�ments
  		this.title = (TextView) findViewById(R.id.instructiondetailsTitle);
  		this.table = (TextView) findViewById(R.id.instructiondetailsTable);
  		this.date = (TextView) findViewById(R.id.instructiondetailsDate);
  		this.instructiondetailsMenuListView = (ListView) findViewById(R.id.instructiondetailsMenusListView);
  		this.finishInstructionButton = (Button) findViewById(R.id.finishInstructionButton);
		
		Long instructionId = getIntent().getExtras().getLong("instructionId");
		this.title.setText("Commande #" + instructionId);
		
		this.finishInstructionButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(InstructionDetailsActivity.this)
		        .setIcon(android.R.drawable.ic_dialog_alert)
		        .setTitle("Confirmation")
		        .setMessage("Voulez-vous vraiment terminer cette commande ?")
		        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		            	finishInstruction(instruction.getId());
		            }
		        })
		        .setNegativeButton("Non", null)
		        .show();
			}
		});
		
		this.getInstruction(instructionId);
	}
	
	
	
	//Tasks
  	//Get instruction
  	private void getInstruction(final Long idInstruction){
  		new AsyncTask<Void, Void, Instruction>(){

  			private String errorMessage;
  			
  			@Override
  			protected Instruction doInBackground(Void... params) {
  				Instruction instruction = null;
  	    		try {
  	    			instruction = InstructionDao.getInstructionById(idInstruction);
  	    		} catch (ResourceNotFoundException e) {
  	    			this.errorMessage = e.getMessage();
  	    			this.cancel(true);
  	    		} catch (JSONException j) {
  	    			this.errorMessage = "Erreur pendant la r�cup�ration des donn�es";
  	    			this.cancel(true);
  	    		}
  	    		return instruction;
  			}
  			
  			protected void onPreExecute() {
  				pd.setMessage("Chargement de la commande...\nVeuillez patienter...");
  				pd.show();
  			}
  			
  			protected void onPostExecute(Instruction result){
  				if(isCancelled()){
  					pd.hide();
  	  				Messages.createAlert(InstructionDetailsActivity.this, "Erreur", Messages.ERROR, errorMessage);
  				} else {
  					pd.hide();
  	  				if(result != null) {
  	  					instruction = result;
  	  					table.setText("Table : " + instruction.getTable().getName());
  	  					date.setText("Pass�e le : " + instruction.getStringDate());
  	  					instructiondetailsMenuListView.setAdapter(new InstructionMenuListAdapter(InstructionDetailsActivity.this, instruction.getMenus()));
  	  				}
  				}
  			}
  			
  		}.execute();
  	}
  	
  	
  	//Finish instruction
  	private void finishInstruction(final Long idInstruction){
  		new AsyncTask<Void, Void, Void>(){

  			private String errorMessage;
  			
  			@Override
  			protected Void doInBackground(Void... params) {
  	    		try {
  	    			InstructionDao.finishInstruction(idInstruction);
  	    		} catch (ResourceNotFoundException e) {
  	    			this.errorMessage = e.getMessage();
  	    			this.cancel(true);
  	    		} catch (JSONException j) {
  	    			this.errorMessage = "Erreur pendant le traitement des donn�es";
  	    			this.cancel(true);
  	    		}
  	    		return null;
  			}
  			
  			protected void onPreExecute() {
  				pd.setMessage("Traitement des donn�es...\nVeuillez patienter...");
  				pd.show();
  			}
  			
  			protected void onPostExecute(Void result){
  				if(isCancelled()){
  					pd.hide();
  					Messages.createAlert(InstructionDetailsActivity.this, "Erreur", Messages.ERROR, errorMessage);
  				} else {
  					pd.hide();
  	  				finish();
  				}
  			}
  			
  		}.execute();
  	}
}
